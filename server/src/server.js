require("dotenv").config();
const express = require("express");

const app = express();

app.get("/", (req, res) => {
  res.send("server");
});

const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`Server started port ${port}`);
});
